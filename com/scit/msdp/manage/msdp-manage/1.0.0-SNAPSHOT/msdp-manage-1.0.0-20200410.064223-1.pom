﻿<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.scit.msdp.manage</groupId>
  <artifactId>msdp-manage</artifactId>
  <version>1.0.0-SNAPSHOT</version>
  <packaging>pom</packaging>
  <name>msdp-manage</name>
  <description>msdp-manage</description>
  
  	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>2.0.6.RELEASE</version>
		<relativePath />
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
			<exclusions> 
	            <exclusion>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-starter-tomcat</artifactId>
	            </exclusion>
            </exclusions>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>com.scit.msdp</groupId>
			<artifactId>msdp-core</artifactId>
		</dependency>
		
		<dependency>
			<groupId>com.scit.msdp</groupId>
			<artifactId>msdp-common</artifactId>
		</dependency>
		
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger2</artifactId>
		</dependency>
		
		<dependency>
			<groupId>io.springfox</groupId>
			<artifactId>springfox-swagger-ui</artifactId>
		</dependency>			
	</dependencies>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>Finchley.SR2</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>

			<dependency>
				<groupId>com.scit.msdp</groupId>
				<artifactId>msdp-core</artifactId>
				<version>1.0.0-SNAPSHOT</version>
			</dependency>
			
			<dependency>
				<groupId>com.scit.msdp</groupId>
				<artifactId>msdp-common</artifactId>
				<version>1.0.0-SNAPSHOT</version>
			</dependency>	
			
			<dependency>
				<groupId>com.scit.msdp</groupId>
				<artifactId>msdp-consul-client</artifactId>
				<version>1.0.0-SNAPSHOT</version>
			</dependency>
			
			<dependency>
				<groupId>com.scit.msdp</groupId>
				<artifactId>msdp-consul-enhance</artifactId>
				<version>1.0.0-SNAPSHOT</version>
			</dependency>

			<dependency>
				<groupId>com.scit.msdp</groupId>
				<artifactId>msdp-mq-kafka</artifactId>
				<version>1.0.0-SNAPSHOT</version>
			</dependency>
			
			<dependency>
				<groupId>com.scit.msdp</groupId>
				<artifactId>msdp-plugins-job</artifactId>
				<version>1.0.0-SNAPSHOT</version>
			</dependency>

			<dependency>
				<groupId>com.scit.msdp</groupId>
				<artifactId>msdp-plugins-verificationcode</artifactId>
				<version>1.0.0-SNAPSHOT</version>
			</dependency>

			<dependency>
				<groupId>com.google.code.gson</groupId>
				<artifactId>gson</artifactId>
				<version>2.8.5</version>
			</dependency>

			<!-- springfox-swagger2 restapi依赖 -->
			<dependency>
				<groupId>io.springfox</groupId>
				<artifactId>springfox-swagger2</artifactId>
				<version>2.9.2</version>
			</dependency>
			<!-- springfox-swagger-ui restapi依赖 -->
			<dependency>
				<groupId>io.springfox</groupId>
				<artifactId>springfox-swagger-ui</artifactId>
				<version>2.9.2</version>
			</dependency>

			<!-- Oracle DB driver依赖 -->
			<dependency>
				<groupId>com.hynnet</groupId>
				<artifactId>oracle-driver-ojdbc</artifactId>
				<version>12.1.0.2</version>
			</dependency>
			<!-- Mysql DB driver依赖 -->
			<dependency>
				<groupId>mysql</groupId>
				<artifactId>mysql-connector-java</artifactId>
				<version>5.1.46</version>
			</dependency>

			<!-- spring-boot mybatis依赖 -->
			<dependency>
				<groupId>org.mybatis.spring.boot</groupId>
				<artifactId>mybatis-spring-boot-starter</artifactId>
				<version>1.3.2</version>
			</dependency>

			<!-- spring-boot mybatis pagehelper依赖 -->
			<dependency>
				<groupId>com.github.pagehelper</groupId>
				<artifactId>pagehelper-spring-boot-starter</artifactId>
				<version>1.2.5</version>
			</dependency>

			<!-- mybatis 自动生成代码 -->
			<dependency>
				<groupId>org.mybatis.generator</groupId>
				<artifactId>mybatis-generator-core</artifactId>
				<version>1.3.6</version>
			</dependency>

			<!-- p6spy 打印完整sql -->
			<dependency>
			    <groupId>p6spy</groupId>
			    <artifactId>p6spy</artifactId>
			    <version>3.8.1</version>
			</dependency>

			<!-- druid springboot -->
			<dependency>
				<groupId>com.alibaba</groupId>
				<artifactId>druid-spring-boot-starter</artifactId>
				<version>1.1.21</version>
			</dependency>

			<!-- alibaba/fastjson -->
			<dependency>
				<groupId>com.alibaba</groupId>
				<artifactId>fastjson</artifactId>
				<version>1.2.47</version>
			</dependency>
			<!-- xml-provider -->
			<dependency>
			    <groupId>com.fasterxml.jackson.jaxrs</groupId>
			    <artifactId>jackson-jaxrs-xml-provider</artifactId>
			    <version>2.9.6</version>
			</dependency>
			<!--bean操作-->
			<dependency>
				<groupId>commons-beanutils</groupId>
				<artifactId>commons-beanutils</artifactId>
				<version>1.9.3</version>
			</dependency>
			<!-- freemarker -->
			<dependency>
				<groupId>org.freemarker</groupId>
				<artifactId>freemarker</artifactId>
				<version>2.3.28</version>
			</dependency>
			<!-- jenkins -->
			<dependency>
			    <groupId>com.offbytwo.jenkins</groupId>
			    <artifactId>jenkins-client</artifactId>
			    <version>0.3.8</version>
			</dependency>
			<!-- ftp -->
			<dependency>
				<groupId>commons-net</groupId>
				<artifactId>commons-net</artifactId>
				<version>3.6</version>
			</dependency>
			<!--docker-->
			<dependency>
				<groupId>com.github.docker-java</groupId>
				<artifactId>docker-java</artifactId>
				<version>3.1.2</version>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<distributionManagement>
		<repository>
			<id>nexus-releases</id>
			<url>
                http://192.168.145.195:8081/repository/maven-releases/
            </url>
		</repository>
		<snapshotRepository>
			<id>nexus-snapshots</id>
			<url>  
                http://192.168.145.195:8081/repository/maven-snapshots/
            </url>
		</snapshotRepository>
	</distributionManagement>
	
  <modules>
  	<module>msdp-manage-common</module>
  	<module>msdp-manage-service</module>
  	<module>msdp-manage-log</module>
  	<module>msdp-manage-deploy</module>
  	<module>msdp-manage-statistics</module>
    <module>msdp-manage-clientlog</module>
  </modules>

     <repositories>
		 <repository>
            <id>kaptcha</id>
            <url>http://maven.jahia.org/maven2/</url>
        </repository>
		 <repository>
			 <id>maven-gitlab</id>
			 <url>https://gitlab.com/shadowedge/maven/raw/master</url>
		 </repository>
    </repositories>
</project>
